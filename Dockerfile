############################################################
# Dockerfile to build Golang project with dep tool
############################################################

FROM golang:1.12.9

# Install supervisor
RUN apt-get update
RUN apt-get install -y supervisor

# Install dep
RUN curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 && chmod +x /usr/local/bin/dep

# Set service directories
RUN mkdir -p /go/src/toko-ijah
RUN mkdir -p /var/log/toko-ijah

WORKDIR /go/src/toko-ijah

# Copy whole code to WORKDIR
COPY . ./

# Install dependencies based on Gopkg.toml and Gopkg.lock
RUN dep ensure -vendor-only

# Build server binary
RUN go build -o main -v main.go

# Run server binary
CMD ["go", "run", "main.go" ]
