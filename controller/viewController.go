package controller

import (
	"net/http"
	"time"
	"toko-ijah/config"
	"toko-ijah/logger"
	"toko-ijah/models"
	"toko-ijah/parameter"
	"toko-ijah/storage"

	"github.com/jinzhu/gorm"
)

//ViewProduct API returns a hardcoded response to check if service is up
func ViewProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ViewProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var products []models.Product
	err := db.Find(&products).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Product Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	config.TPL.ExecuteTemplate(w, "product.gohtml", products)
}

//ViewIncomingProduct API returns a hardcoded response to check if service is up
func ViewIncomingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ViewIncomingProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var incomingProducts []models.ProductIn
	err := db.Preload("Product").Find(&incomingProducts).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "ProductIn Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	config.TPL.ExecuteTemplate(w, "productIncoming.gohtml", incomingProducts)
}

//ViewOutgoingProduct API returns a hardcoded response to check if service is up
func ViewOutgoingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ViewOutgoingProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var outgoingProducts []models.ProductOut
	err := db.Preload("Product").Find(&outgoingProducts).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "ProductOut Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	config.TPL.ExecuteTemplate(w, "productOutgoing.gohtml", outgoingProducts)
}

//ViewReportProductValue API returns a hardcoded response to check if service is up
func ViewReportProductValue(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ViewReportProductValue api")

	productValueMap, totalQuantity, totalPrice, uniqueSKU, err := getProductValues()
	if err == gorm.ErrRecordNotFound {
		logger.Log.Println(err)
		http.Error(w, "Products Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		logger.Log.Println(err)
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	var report parameter.ProductValueReport
	report.ProductValues = productValueMap
	report.CreationDate = time.Now().Format("02 January 2006")
	report.TotalQuantity = totalQuantity
	report.TotalPrice = totalPrice
	report.UniqueSKU = uniqueSKU

	config.TPL.ExecuteTemplate(w, "reportProductValue.gohtml", report)
}
