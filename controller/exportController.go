package controller

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"strconv"
	"time"
	"toko-ijah/logger"
	"toko-ijah/models"
	"toko-ijah/storage"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

//ExportProduct API returns a hardcoded response to check if service is up
func ExportProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ExportProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var products []models.Product
	err := db.Order("sku").Find(&products).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Product Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	csvContent := [][]string{}
	for _, product := range products {
		csvContent = append(csvContent, []string{
			product.SKU,
			product.Name,
			strconv.Itoa(product.Quantity),
		})
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment;filename=CatatanJumlahBarang.csv")
	wr := csv.NewWriter(w)
	for _, value := range csvContent {
		err = wr.Write(value)
		if err != nil {
			http.Error(w, "Error sending csv: "+err.Error(), http.StatusInternalServerError)
			return
		}
	}
	wr.Flush()
}

//ExportIncomingProduct API returns a hardcoded response to check if service is up
func ExportIncomingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ExportIncomingProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var incomingProducts []models.ProductIn
	err := db.Order("receipt_timestamp desc").Preload("Product").Find(&incomingProducts).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "ProductIn Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	csvContent := [][]string{}
	for _, incomingProduct := range incomingProducts {
		var product models.Product
		err = db.Model(&incomingProduct).Related(&product).Error
		if err != nil {
			http.Error(w, "Something went wrong", http.StatusInternalServerError)
			return
		}
		csvContent = append(csvContent, []string{
			incomingProduct.ReceiptTimestamp.Format("2006/01/02 3:04"),
			product.SKU,
			product.Name,
			strconv.Itoa(incomingProduct.Ordered),
			strconv.Itoa(incomingProduct.Received),
			fmt.Sprintf("%f", incomingProduct.UnitPrice),
			fmt.Sprintf("%f", incomingProduct.TotalPrice),
			incomingProduct.ReceiptID,
			incomingProduct.Comments,
		})
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment;filename=CatatanBarangMasuk.csv")
	wr := csv.NewWriter(w)
	for _, value := range csvContent {
		err = wr.Write(value)
		if err != nil {
			http.Error(w, "Error sending csv: "+err.Error(), http.StatusInternalServerError)
			return
		}
	}
	wr.Flush()
}

//ExportOutgoingProduct API returns a hardcoded response to check if service is up
func ExportOutgoingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ExportOutgoingProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var outgoingProducts []models.ProductOut
	err := db.Order("order_timestamp desc").Preload("Product").Find(&outgoingProducts).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Product Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		logger.Log.Println(err)
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	csvContent := [][]string{}
	for _, outgoingProduct := range outgoingProducts {
		var product models.Product
		err = db.Model(&outgoingProduct).Related(&product).Error
		if err != nil {
			http.Error(w, "Something went wrong", http.StatusInternalServerError)
			return
		}
		csvContent = append(csvContent, []string{
			outgoingProduct.OrderTimestamp.Format("2006-01-02 3:04:05"),
			product.SKU,
			product.Name,
			strconv.Itoa(outgoingProduct.Quantity),
			fmt.Sprintf("%f", outgoingProduct.UnitPrice),
			fmt.Sprintf("%f", outgoingProduct.TotalPrice),
			outgoingProduct.Comments,
		})
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment;filename=CatatanBarangKeluar.csv")
	wr := csv.NewWriter(w)
	for _, value := range csvContent {
		err = wr.Write(value)
		if err != nil {
			http.Error(w, "Error sending csv: "+err.Error(), http.StatusInternalServerError)
			return
		}
	}
	wr.Flush()
}

//ExportReportProductValue API returns a response to store outgoing product
func ExportReportProductValue(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ExportReportProductValue api")

	productValueMap, totalQuantity, totalPrice, uniqueSKU, err := getProductValues()
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Products Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong product value", http.StatusInternalServerError)
		return
	}

	csvContent := [][]string{
		[]string{time.Now().Format("02 January 2006")},
		[]string{strconv.Itoa(uniqueSKU)},
		[]string{strconv.Itoa(totalQuantity)},
		[]string{fmt.Sprintf("%f", totalPrice)},
	}

	for _, productValue := range productValueMap {
		csvContent = append(csvContent, []string{
			productValue.SKU,
			productValue.Name,
			strconv.Itoa(productValue.Quantity),
			fmt.Sprintf("%f", productValue.MedianPrice),
			fmt.Sprintf("%f", productValue.TotalPrice)})
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment;filename=LaporanNilaiBarang.csv")
	wr := csv.NewWriter(w)
	for _, value := range csvContent {
		err = wr.Write(value)
		if err != nil {
			http.Error(w, "Error sending csv: "+err.Error(), http.StatusInternalServerError)
			return
		}
	}
	wr.Flush()
}

//ExportReportProductSales API returns a response to store outgoing product
func ExportReportProductSales(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ExportReportProductSales api")

	vars := mux.Vars(r)
	startDate := vars["startDate"]
	endDate := vars["endDate"]

	layout := "2006-01-02"
	sDate, err := time.Parse(layout, startDate)
	if err != nil {
		logger.Log.Println(err)
		http.Error(w, "startDate format is YYYY-MM-DD", http.StatusInternalServerError)
		return
	}

	eDate, err := time.Parse(layout, endDate)
	if err != nil {
		logger.Log.Println(err)
		http.Error(w, "endDate format is YYYY-MM-DD", http.StatusInternalServerError)
		return
	}

	sales, totalSales, totalGross, totalOrder, totalQuantity, err := getProductSales(sDate, eDate)
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Products Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	csvContent := [][]string{
		[]string{time.Now().Format("2 January 2006")},
		[]string{sDate.Format("2 January 2006") + " - " + eDate.Format("2 January 2006")},
		[]string{fmt.Sprintf("%f", totalSales)},
		[]string{fmt.Sprintf("%f", totalGross)},
		[]string{strconv.Itoa(totalOrder)},
		[]string{strconv.Itoa(totalQuantity)},
	}

	for _, sale := range sales {
		csvContent = append(csvContent, []string{
			sale.OrderID,
			sale.OrderTimestamp,
			sale.SKU,
			sale.Name,
			strconv.Itoa(sale.Quantity),
			fmt.Sprintf("%f", sale.OutgoingPrice),
			fmt.Sprintf("%f", sale.TotalOutgoingPrice),
			fmt.Sprintf("%f", sale.IncomingPrice),
			fmt.Sprintf("%f", sale.TotalGrossPrice),
		})
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment;filename=LaporanPenjualan.csv")
	wr := csv.NewWriter(w)
	for _, value := range csvContent {
		err = wr.Write(value)
		if err != nil {
			http.Error(w, "Error sending csv: "+err.Error(), http.StatusInternalServerError)
			return
		}
	}
	wr.Flush()

}
