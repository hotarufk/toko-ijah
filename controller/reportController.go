package controller

import (
	"encoding/json"
	"net/http"
	"time"
	"toko-ijah/logger"
	"toko-ijah/parameter"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

//ReportProductValue API returns a response to store outgoing product
func ReportProductValue(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ReportProductValue api")

	productValueMap, totalQuantity, totalPrice, uniqueSKU, err := getProductValues()
	if err == gorm.ErrRecordNotFound {
		logger.Log.Println(err)
		http.Error(w, "Products Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		logger.Log.Println(err)
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	var report parameter.ProductValueReport
	report.ProductValues = productValueMap
	report.CreationDate = time.Now().Format("02 January 2006")
	report.TotalQuantity = totalQuantity
	report.TotalPrice = totalPrice
	report.UniqueSKU = uniqueSKU

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(report)
}

//ReportProductSales API returns a response to store outgoing product
func ReportProductSales(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ReportProductSales api")

	vars := mux.Vars(r)
	startDate := vars["startDate"]
	endDate := vars["endDate"]

	layout := "2006-01-02"
	sDate, err := time.Parse(layout, startDate)
	if err != nil {
		logger.Log.Println(err)
		http.Error(w, "startDate format is YYYY-MM-DD", http.StatusInternalServerError)
		return
	}

	eDate, err := time.Parse(layout, endDate)
	if err != nil {
		logger.Log.Println(err)
		http.Error(w, "endDate format is YYYY-MM-DD", http.StatusInternalServerError)
		return
	}

	sales, totalSales, totalGross, totalOrder, totalQuantity, err := getProductSales(sDate, eDate)
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Products Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}
	var report parameter.SalesReport
	report.Sales = sales
	report.CreationDate = time.Now().Format("2 January 2006")
	report.DateRange = sDate.Format("2 January 2006") + " - " + eDate.Format("2 January 2006")
	report.TotalSales = totalSales
	report.TotalGross = totalGross
	report.TotalOrder = totalOrder
	report.TotalQuantity = totalQuantity
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(report)

}
