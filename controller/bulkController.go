package controller

import (
	"encoding/json"
	"net/http"
	"toko-ijah/logger"
	"toko-ijah/models"
	"toko-ijah/storage"

	"github.com/jinzhu/gorm"
)

//GetAllProduct API returns a hardcoded response to check if service is up
func GetAllProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside GetAllProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var products []models.Product
	err := db.Find(&products).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Product Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(products)
}

//GetAllIncomingProduct API returns a hardcoded response to check if service is up
func GetAllIncomingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside GetAllIncomingProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var incomingProducts []models.ProductIn
	err := db.Preload("Product").Find(&incomingProducts).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "ProductIn Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(incomingProducts)
}

//GetAllOutgoingProduct API returns a hardcoded response to check if service is up
func GetAllOutgoingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside GetAllOutgoingProduct api")

	db := storage.SqliteConn()
	defer db.Close()

	var outgoingProducts []models.ProductOut
	err := db.Preload("Product").Find(&outgoingProducts).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "ProductIn Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		logger.Log.Println(err)
		http.Error(w, "Something went wrong", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(outgoingProducts)
}
