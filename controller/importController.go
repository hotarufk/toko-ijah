package controller

import (
	"encoding/csv"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"toko-ijah/logger"
	"toko-ijah/parameter"
)

//ImportProduct API returns a hardcoded response to check if service is up
func ImportProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ImportProduct api")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("data")
	if err != nil {
		logger.Log.Println("Error Retrieving the File")
		logger.Log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer file.Close()

	fileExtension := filepath.Ext(handler.Filename)
	logger.Log.Printf("File : %v extension :%v", handler.Filename, fileExtension)
	extList := []string{".csv"}
	isValidExt := false
	for _, v := range extList {
		if v == strings.ToLower(fileExtension) {
			isValidExt = true
			break
		}
	}

	if !isValidExt {
		logger.Log.Println("File type incorrect")
		http.Error(w, "File type incorrect", http.StatusPreconditionFailed)
		return
	}

	lines, err := csv.NewReader(file).ReadAll()
	if err != nil {
		logger.Log.Println(err)
		http.Error(w, "Error when reading file", http.StatusPreconditionFailed)
		return
	}

	for i, line := range lines {
		if len(line) != 3 {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return
		}
		sku := line[0]
		name := line[1]
		logger.Log.Println("Try creating product for sku " + sku + " and name" + name)

		// IGNORE LINE 3
		_, err := createProduct(sku, name)
		if err != nil && err.Error() != "Product SKU "+sku+" already exist in DB" {
			logger.Log.Println("Error when creating product for sku " + sku + " and name" + name + " :" + err.Error())
			http.Error(w, err.Error(), http.StatusPreconditionFailed)
			return
		}
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Product sucessfully imported"))
}

//ImportIncomingProduct API returns a hardcoded response to check if service is up
func ImportIncomingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ImportIncomingProduct api")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("data")
	if err != nil {
		logger.Log.Println("Error Retrieving the File")
		logger.Log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer file.Close()

	fileExtension := filepath.Ext(handler.Filename)
	logger.Log.Printf("File : %v extension :%v", handler.Filename, fileExtension)
	extList := []string{".csv"}
	isValidExt := false
	for _, v := range extList {
		if v == strings.ToLower(fileExtension) {
			isValidExt = true
			break
		}
	}

	if !isValidExt {
		logger.Log.Println("File type incorrect")
		http.Error(w, "File type incorrect", http.StatusPreconditionFailed)
		return
	}

	lines, err := csv.NewReader(file).ReadAll()
	if err != nil {
		logger.Log.Println(err)
		http.Error(w, "Error when reading file", http.StatusPreconditionFailed)
		return
	}

	for i, line := range lines {
		if len(line) != 9 {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return
		}

		orderedQuantity, err := strconv.Atoi(line[3])
		if err != nil {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return

		}

		receivedQuantity, err := strconv.Atoi(line[4])
		if err != nil {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return

		}

		unitPrice, err := strconv.ParseFloat(line[5], 32)
		if err != nil {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return
		}

		var param parameter.IncomingProduct
		param.ReceiptTimestamp = line[0]
		param.SKU = line[1]
		param.OrderedQuantity = orderedQuantity
		param.ReceivedQuantity = receivedQuantity
		param.UnitPrice = float32(unitPrice)
		param.ReceiptID = line[7]
		param.Comments = line[8]
		logger.Log.Println("Try creating productin with data :", param)

		_, err = createProductIn(param)
		if err != nil {
			logger.Log.Println("Error when creating productin :" + err.Error())
			http.Error(w, err.Error(), http.StatusPreconditionFailed)
			return
		}
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ProductIn sucessfully imported"))
}

//ImportOutgoingProduct API returns a hardcoded response to check if service is up
func ImportOutgoingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside ImportOutgoingProduct api")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("data")
	if err != nil {
		logger.Log.Println("Error Retrieving the File")
		logger.Log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer file.Close()

	fileExtension := filepath.Ext(handler.Filename)
	logger.Log.Printf("File : %v extension :%v", handler.Filename, fileExtension)
	extList := []string{".csv"}
	isValidExt := false
	for _, v := range extList {
		if v == strings.ToLower(fileExtension) {
			isValidExt = true
			break
		}
	}

	if !isValidExt {
		logger.Log.Println("File type incorrect")
		http.Error(w, "File type incorrect", http.StatusPreconditionFailed)
		return
	}

	lines, err := csv.NewReader(file).ReadAll()
	if err != nil {
		logger.Log.Println(err)
		http.Error(w, "Error when reading file", http.StatusPreconditionFailed)
		return
	}

	for i, line := range lines {
		if len(line) != 7 {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return
		}

		quantity, err := strconv.Atoi(line[3])
		if err != nil {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return

		}

		unitPrice, err := strconv.ParseFloat(line[4], 32)
		if err != nil {
			msg := "Error when reading csv file, incorrect format in row " + strconv.Itoa(1+i)
			logger.Log.Println(msg)
			http.Error(w, msg, http.StatusPreconditionFailed)
			return
		}

		comment := line[6]
		commentList := strings.Split(comment, "Pesanan ")
		orderID := ""
		if len(commentList) > 1 {
			orderID = commentList[1]
		}

		var param parameter.OutgoingProduct
		param.OrderTimestamp = line[0]
		param.SKU = line[1]
		param.Quantity = quantity
		param.UnitPrice = float32(unitPrice)
		param.Comments = comment
		param.OrderID = orderID
		logger.Log.Println("Try creating productin with data :", param)

		_, err = createProductOut(param)
		if err != nil {
			logger.Log.Println("Error when creating productOut :" + err.Error())
			http.Error(w, err.Error(), http.StatusPreconditionFailed)
			return
		}
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ProductOut sucessfully imported"))
}
