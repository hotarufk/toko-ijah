package controller

import (
	"net/http"

	"github.com/gorilla/mux"
)

//GetRoute gets the router pointing to all API functions
func GetRoute() *mux.Router {
	r := mux.NewRouter()
	r.StrictSlash(true)

	r.HandleFunc("/", ViewProduct).Methods(http.MethodGet)
	r.HandleFunc("/incoming", ViewIncomingProduct).Methods(http.MethodGet)
	r.HandleFunc("/outgoing", ViewOutgoingProduct).Methods(http.MethodGet)
	r.HandleFunc("/reportValue", ViewReportProductValue).Methods(http.MethodGet)

	r.HandleFunc("/product", GetAllProduct).Methods(http.MethodGet)
	r.HandleFunc("/product", RegisterProduct).Methods(http.MethodPost)
	r.HandleFunc("/product/export", ExportProduct).Methods(http.MethodGet)
	r.HandleFunc("/product/import", ImportProduct).Methods(http.MethodPost)

	r.HandleFunc("/product/incoming", GetAllIncomingProduct).Methods(http.MethodGet)
	r.HandleFunc("/product/incoming", IncomingProduct).Methods(http.MethodPost)
	r.HandleFunc("/product/incoming/export", ExportIncomingProduct).Methods(http.MethodGet)
	r.HandleFunc("/product/incoming/import", ImportIncomingProduct).Methods(http.MethodPost)

	r.HandleFunc("/product/outgoing", GetAllOutgoingProduct).Methods(http.MethodGet)
	r.HandleFunc("/product/outgoing", OutgoingProduct).Methods(http.MethodPost)
	r.HandleFunc("/product/outgoing/export", ExportOutgoingProduct).Methods(http.MethodGet)
	r.HandleFunc("/product/outgoing/import", ImportOutgoingProduct).Methods(http.MethodPost)

	r.HandleFunc("/product/report/value", ReportProductValue).Methods(http.MethodGet)
	r.HandleFunc("/product/report/value/export", ExportReportProductValue).Methods(http.MethodGet)

	r.HandleFunc("/product/report/sales/{startDate}/{endDate}", ReportProductSales).Methods(http.MethodGet)
	r.HandleFunc("/product/report/sales/{startDate}/{endDate}/export", ExportReportProductSales).Methods(http.MethodGet)

	r.HandleFunc("/product/{sku}", GetProduct).Methods(http.MethodGet)
	return r
}
