package controller

import (
	"encoding/json"
	"net/http"
	"toko-ijah/logger"
	"toko-ijah/models"
	"toko-ijah/parameter"
	"toko-ijah/storage"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

//RegisterProduct API returns a hardcoded response to check if service is up
func RegisterProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside RegisterProduct API")

	var reqParams parameter.RegisterProduct
	//validate request params and map it to struct
	if err := parameter.ParseRequest(r, &reqParams); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	logger.Log.Println("request params: ", reqParams)

	product, err := createProduct(reqParams.SKU, reqParams.Name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusPreconditionFailed)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(product)
	return
}

//GetProduct API returns a hardcoded response to check if service is up
func GetProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside GetProduct api")

	vars := mux.Vars(r)
	productSKU := vars["sku"]

	db := storage.SqliteConn()
	defer db.Close()

	var product models.Product
	err := db.First(&product, models.Product{SKU: productSKU}).Error
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Product Not Found", http.StatusNotFound)
		return
	}

	if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong :"+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(product)
}

//IncomingProduct API returns a hardcoded response to check if service is up
func IncomingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside IncomingProduct api")

	var reqParams parameter.IncomingProduct
	//validate request params and map it to struct
	if err := parameter.ParseRequest(r, &reqParams); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	logger.Log.Println("request params: ", reqParams)

	productIn, err := createProductIn(reqParams)
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Product Not Found", http.StatusPreconditionFailed)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong :"+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(productIn)
}

//OutgoingProduct API returns a response to store outgoing product
func OutgoingProduct(w http.ResponseWriter, r *http.Request) {
	logger.Log.Println("inside OutgoingProduct api")

	var reqParams parameter.OutgoingProduct
	//validate request params and map it to struct
	if err := parameter.ParseRequest(r, &reqParams); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	logger.Log.Println(w, "request params: %v", reqParams)

	productOut, err := createProductOut(reqParams)
	if err == gorm.ErrRecordNotFound {
		http.Error(w, "Product Not Found", http.StatusNotFound)
		return
	} else if err != gorm.ErrRecordNotFound && err != nil {
		http.Error(w, "Something went wrong :"+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(productOut)
}
