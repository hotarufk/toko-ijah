package controller

import (
	"errors"
	"time"
	"toko-ijah/logger"
	"toko-ijah/models"
	"toko-ijah/parameter"
	"toko-ijah/storage"

	"github.com/jinzhu/gorm"
)

func createProduct(sku string, name string) (product models.Product, err error) {
	db := storage.SqliteConn()
	defer db.Close()

	err = db.First(&product, models.Product{SKU: sku}).Error
	if err != nil {
		if err != gorm.ErrRecordNotFound {
			logger.Log.Println("Error while checking is product exist : " + err.Error())
			return product, err
		}

		logger.Log.Println("Product Sku" + sku + "not exist, creating new product")
		product = models.Product{SKU: sku, Name: name}
		if err = db.Create(&product).Error; err != nil {
			logger.Log.Println("Error while creating product: " + err.Error())
			return product, err
		}

	} else {
		// PRODUCT ALREADY EXIST IN DB
		msg := "Product SKU " + sku + " already exist in DB"
		logger.Log.Println(msg)
		err = errors.New(msg)
	}
	return product, err
}

func createProductIn(param parameter.IncomingProduct) (productIn models.ProductIn, err error) {
	db := storage.SqliteConn()
	defer db.Close()

	var product models.Product
	err = db.First(&product, models.Product{SKU: param.SKU}).Error
	if err == gorm.ErrRecordNotFound {
		logger.Log.Println("Error product with sku " + param.SKU + " not found")
		return productIn, err
	} else if err != gorm.ErrRecordNotFound && err != nil {
		logger.Log.Println("Error when searching productwith sku " + param.SKU + " : " + err.Error())
		return productIn, err
	}

	layout := "2006/01/02 3:04"
	receiptTimestamp, err := time.Parse(layout, param.ReceiptTimestamp)
	if err != nil {
		msg := "Receipt Timestamp format is YYYY/MM/DD K:mm"
		err = errors.New(msg)
		logger.Log.Println(msg)
		return productIn, err
	}

	var totalPrice float32
	totalPrice = param.UnitPrice * float32(param.ReceivedQuantity)

	productIn = models.ProductIn{
		ProductID:        product.ID,
		Product:          product,
		ReceiptID:        param.ReceiptID,
		ReceiptTimestamp: receiptTimestamp,
		Ordered:          param.OrderedQuantity,
		Received:         param.ReceivedQuantity,
		UnitPrice:        param.UnitPrice,
		TotalPrice:       totalPrice,
		Comments:         param.Comments}
	if err := db.Create(&productIn).Error; err != nil {
		logger.Log.Println("Error while creating productIn: ", err)
		return productIn, err
	}

	err = db.Model(&product).Updates(map[string]interface{}{"quantity": product.Quantity + productIn.Received}).Error
	if err != nil {
		logger.Log.Println("Error while updating product quantity: ", err)
		return productIn, err
	}

	return productIn, err
}

func createProductOut(param parameter.OutgoingProduct) (productOut models.ProductOut, err error) {
	db := storage.SqliteConn()
	defer db.Close()

	var product models.Product
	err = db.First(&product, models.Product{SKU: param.SKU}).Error
	if err == gorm.ErrRecordNotFound {
		logger.Log.Println("Error while checking is product exist : " + err.Error())
		return productOut, err
	} else if err != gorm.ErrRecordNotFound && err != nil {
		logger.Log.Println("Error when searching productwith sku " + param.SKU + " : " + err.Error())
		return productOut, err
	}

	layout := "2006-01-02 3:04:05"
	orderTimestamp, err := time.Parse(layout, param.OrderTimestamp)
	if err != nil {
		msg := "Receipt Timestamp format is YYYY-MM=DD K:mm:ss"
		err = errors.New(msg)
		logger.Log.Println(msg)
		return productOut, err
	}

	unitPrice := param.UnitPrice
	comments := param.Comments
	if param.OrderID == "" {
		unitPrice = float32(0)
		if comments == "" {
			msg := "Comment shoulbe filled for non sales product outgoing"
			err = errors.New(msg)
			logger.Log.Println(msg)
			return productOut, err
		}
	} else if param.UnitPrice == float32(0) {
		msg := "UnitPrice should be bigger than 0"
		err = errors.New(msg)
		logger.Log.Println(msg)
		return productOut, err
	} else {
		comments = "Pesananan " + param.OrderID
	}
	var totalPrice float32
	totalPrice = unitPrice * float32(param.Quantity)
	productOut = models.ProductOut{
		ProductID:      product.ID,
		Product:        product,
		OrderID:        param.OrderID,
		OrderTimestamp: orderTimestamp,
		Quantity:       param.Quantity,
		UnitPrice:      unitPrice,
		TotalPrice:     totalPrice,
		Comments:       comments}
	if err := db.Create(&productOut).Error; err != nil {
		logger.Log.Println("Error while creating productOut: ", err)
		return productOut, err
	}

	err = db.Model(&product).Updates(map[string]interface{}{"quantity": product.Quantity - productOut.Quantity}).Error
	if err != nil {
		logger.Log.Println("Error while updating product quantity: ", err)
		return productOut, err
	}
	return productOut, err
}

func getProductValues() (map[string]parameter.ProductValue, int, float32, int, error) {
	var productValueMap map[string]parameter.ProductValue
	productValueMap = map[string]parameter.ProductValue{}
	totalQuantity, totalPrice, uniqueSKU := 0, float32(0), 0

	db := storage.SqliteConn()
	defer db.Close()

	var products []models.Product
	err := db.Order("sku").Find(&products).Error
	if err != nil {
		logger.Log.Println("Error while getting product: ", err)
		return productValueMap, totalQuantity, totalPrice, uniqueSKU, err
	}

	for _, product := range products {
		var productValue parameter.ProductValue
		productValue, err = product.GetMedianPrice()
		if err != nil {
			logger.Log.Println("Error while getting median price: ", err)
			return productValueMap, totalQuantity, totalPrice, uniqueSKU, err
		}
		productValueMap[productValue.SKU] = productValue

		productValueMap[productValue.SKU] = productValue
		totalQuantity += productValue.Quantity
		totalPrice += productValue.TotalPrice
		uniqueSKU++
	}
	return productValueMap, totalQuantity, totalPrice, uniqueSKU, nil
}

func getProductSales(sDate time.Time, eDate time.Time) (sales []parameter.Sale, totalSales float32, totalGross float32, totalOrder int, totalQuantity int, err error) {
	productValueMap, _, _, _, err := getProductValues()
	if err == gorm.ErrRecordNotFound {
		return sales, totalSales, totalGross, totalOrder, totalQuantity, err
	} else if err != gorm.ErrRecordNotFound && err != nil {
		return sales, totalSales, totalGross, totalOrder, totalQuantity, err
	}

	db := storage.SqliteConn()
	defer db.Close()

	var productOutgoings []models.ProductOut
	err = db.Order("order_timestamp desc, order_id, product_id").Where("order_timestamp BETWEEN ? AND ?", sDate, eDate).Not("order_id", "").Find(&productOutgoings).Error
	if err == gorm.ErrRecordNotFound {
		return sales, totalSales, totalGross, totalOrder, totalQuantity, err
	} else if err != gorm.ErrRecordNotFound && err != nil {
		return sales, totalSales, totalGross, totalOrder, totalQuantity, err
	}

	orders := map[string]int{}
	for _, productOutgoing := range productOutgoings {
		var product models.Product
		err = db.Model(&productOutgoing).Related(&product).Error
		incomingPrice := productValueMap[product.SKU].MedianPrice
		totalIncomingPrice := incomingPrice * float32(productOutgoing.Quantity)
		sale := parameter.Sale{
			OrderID:            productOutgoing.OrderID,
			OrderTimestamp:     productOutgoing.OrderTimestamp.Format("2006-01-02 3:04:05"),
			SKU:                product.SKU,
			Name:               product.Name,
			Quantity:           productOutgoing.Quantity,
			OutgoingPrice:      productOutgoing.UnitPrice,
			TotalOutgoingPrice: productOutgoing.TotalPrice,
			IncomingPrice:      incomingPrice,
			TotalGrossPrice:    productOutgoing.TotalPrice - totalIncomingPrice}
		sales = append(sales, sale)
		totalSales += sale.TotalOutgoingPrice
		totalGross += sale.TotalGrossPrice
		if orders[product.SKU] == 0 {
			totalOrder++
			orders[product.SKU]++
		}
		totalQuantity += sale.Quantity
	}
	return sales, totalSales, totalGross, totalOrder, totalQuantity, nil
}
