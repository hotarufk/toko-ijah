package storage

import (
	"database/sql"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3" //Required to connect to sqlite
	"github.com/pkg/errors"
)

//SqliteConn get db instance using gorm for query using orm
func SqliteConn() *gorm.DB {
	db, err := gorm.Open("sqlite3", "database.db")
	if err != nil {
		panic(errors.Wrap(err, "Cannot Connect to DB"))
	}
	return db
}

//RawSqliteConn get db instance using sql for raw query
func RawSqliteConn() *sql.DB {
	db, err := sql.Open("sqlite3", "database.db")
	if err != nil {
		panic(errors.Wrap(err, "Cannot Connect to DB"))
	}
	return db
}
