package parameter

//ProductValueReport to store report for product value report
type ProductValueReport struct {
	CreationDate  string                  `json:"creation_date" valid:"required"`
	UniqueSKU     int                     `json:"unique_sku" valid:"required"`
	TotalQuantity int                     `json:"total_quantity" valid:"required"`
	TotalPrice    float32                 `json:"total_price" valid:"required"`
	ProductValues map[string]ProductValue `json:"product_values" valid:"required"`
}

//ProductValue to store product value
type ProductValue struct {
	SKU         string  `json:"sku" valid:"required"`
	Name        string  `json:"name" valid:"required"`
	Quantity    int     `json:"quantity" valid:"required"`
	MedianPrice float32 `json:"media_price" valid:"required"`
	TotalPrice  float32 `json:"total_price" valid:"required"`
}

//SalesReport to store report for sales in certain period
type SalesReport struct {
	CreationDate  string  `json:"creation_date" valid:"required"`
	DateRange     string  `json:"date_range" valid:"required"`
	TotalSales    float32 `json:"total_sales" valid:"required"`
	TotalGross    float32 `json:"total_gross" valid:"required"`
	TotalOrder    int     `json:"total_order" valid:"required"`
	TotalQuantity int     `json:"total_quantity" valid:"required"`
	Sales         []Sale  `json:"sales" valid:"required"`
}

//Sale to store sales for each product in each order id
type Sale struct {
	OrderID            string  `json:"order_id" valid:"required"`
	OrderTimestamp     string  `json:"order_timestamp" valid:"required"`
	SKU                string  `json:"sku" valid:"required"`
	Name               string  `json:"name" valid:"required"`
	Quantity           int     `json:"quantity" valid:"required"`
	OutgoingPrice      float32 `json:"outgoing_price" valid:"required"`
	TotalOutgoingPrice float32 `json:"total_outgoing_price" valid:"required"`
	IncomingPrice      float32 `json:"incoming_price" valid:"required"`
	TotalGrossPrice    float32 `json:"total_gross_price" valid:"required"`
}
