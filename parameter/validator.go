package parameter

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"toko-ijah/logger"

	"github.com/asaskevich/govalidator"
)

//ParseRequest allows reading from request body multiple times and mapping it to struct with param validation
func ParseRequest(r *http.Request, params interface{}) error {
	// When there are multiple middlewares, reading once from Body
	// will close the reader and that panics on subsequent ParseRequest calls.
	// Save the request in a Buf and then share over two states 1 and 2.
	// Assign one of them back to request.Body. Simple.

	buf, _ := ioutil.ReadAll(r.Body)
	rdr1 := ioutil.NopCloser(bytes.NewBuffer(buf))
	rdr2 := ioutil.NopCloser(bytes.NewBuffer(buf))
	r.Body = rdr2

	//check if request is parseable
	decoder := json.NewDecoder(rdr1)
	if err := decoder.Decode(&params); err != nil {
		logger.Log.Printf("request decode failed: %v", err)
		return fmt.Errorf("cannot decode the request: %v", err)
	}

	//validate params based on struct tags
	if ok, err := govalidator.ValidateStruct(params); !ok {
		logger.Log.Printf("request validation failed: %v", err)
		return err
	}
	return nil
}
