package parameter

//RegisterProduct to accept POST params for registering product
type RegisterProduct struct {
	SKU  string `json:"sku" valid:"required"`
	Name string `json:"name" valid:"required"`
}

//IncomingProduct to accept POST params for restock product
type IncomingProduct struct {
	SKU              string  `json:"sku" valid:"required"`
	OrderedQuantity  int     `json:"ordered_quantity" valid:"required"`
	ReceivedQuantity int     `json:"received_quantity" valid:"required"`
	UnitPrice        float32 `json:"unit_price" valid:"required"`
	ReceiptID        string  `json:"receipt_id" valid:"required"`
	ReceiptTimestamp string  `json:"receipt_timestamp" valid:"required"`
	Comments         string  `json:"comment" valid:"required"`
}

//OutgoingProduct to accept POST params for restock product
type OutgoingProduct struct {
	SKU            string  `json:"sku" valid:"required"`
	Quantity       int     `json:"quantity" valid:"required"`
	UnitPrice      float32 `json:"unit_price"`
	OrderID        string  `json:"order_id"`
	OrderTimestamp string  `json:"order_timestamp" valid:"required"`
	Comments       string  `json:"comment"`
}
