package models

import (
	"time"
	"toko-ijah/logger"
	"toko-ijah/storage"

	"github.com/jinzhu/gorm"
)

// ProductIn model to store  ijah product
type ProductIn struct {
	gorm.Model
	ReceiptID        string    `gorm:"not null"`
	ReceiptTimestamp time.Time `gorm:"not null"`
	Product          Product   `gorm:"foreignkey:ProductID"`
	ProductID        uint      `gorm:"not null"`
	Ordered          int       `gorm:"not null"`
	Received         int       `gorm:"not null"`
	UnitPrice        float32   `gorm:"not null"`
	TotalPrice       float32   `gorm:"not null"`
	Comments         string    `gorm:"not null"`
}

//Migrate ProductIn model structure to db
func (ProductIn) migrate() {
	logger.Log.Println("migrating product_in schema")
	db := storage.SqliteConn()
	db.AutoMigrate(ProductIn{})
}

//TableName return name of databse table used for this model
func (ProductIn) TableName() string {
	return "product_in"
}
