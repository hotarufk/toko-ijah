package models

import (
	"toko-ijah/logger"
	"toko-ijah/parameter"
	"toko-ijah/storage"

	"github.com/jinzhu/gorm"
)

// Product model to store  ijah product
type Product struct {
	gorm.Model
	SKU      string `gorm:"not null"`
	Name     string `gorm:"not null"`
	Quantity int    `gorm:"default:0 not null"`
}

//Migrate Product model structure to db
func (Product) migrate() {
	logger.Log.Println("migrating product schema")
	db := storage.SqliteConn()
	db.AutoMigrate(Product{})
}

//TableName return name of databse table used for this model
func (Product) TableName() string {
	return "product"
}

//GetMedianPrice model structure to db
func (p Product) GetMedianPrice() (parameter.ProductValue, error) {
	var productValue parameter.ProductValue

	db := storage.SqliteConn()
	defer db.Close()

	var productIncomings []ProductIn
	err := db.Where(ProductIn{ProductID: p.ID}).Find(&productIncomings).Error
	if err != nil {
		return productValue, err
	}

	for _, productIncoming := range productIncomings {
		productValue.Quantity += productIncoming.Received
		productValue.TotalPrice += productIncoming.TotalPrice
	}

	medianPrice := float32(0)
	if productValue.Quantity != 0 {
		medianPrice = productValue.TotalPrice / float32(productValue.Quantity)
	}
	productValue.SKU = p.SKU
	productValue.Name = p.Name
	productValue.MedianPrice = medianPrice

	return productValue, nil
}
