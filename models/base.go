package models

import "toko-ijah/logger"

//BaseModel gives basic migrate method where each model can define their own schema
type BaseModel interface {
	migrate()
}

//Migrate creates all the DB tables with their own constraints
func Migrate() {
	logger.Log.Println("migrating schema")
	allModels := []BaseModel{Product{}, ProductIn{}, ProductOut{}}
	for _, model := range allModels {
		model.migrate()
	}
}
