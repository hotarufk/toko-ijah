package models

import (
	"time"
	"toko-ijah/logger"
	"toko-ijah/storage"

	"github.com/jinzhu/gorm"
)

// ProductOut model to store  ijah product
type ProductOut struct {
	gorm.Model
	OrderID        string    `gorm:"not null"`
	OrderTimestamp time.Time `gorm:"not null"`
	Product        Product   `gorm:"foreignkey:ProductID"`
	ProductID      uint      `gorm:"not null"`
	Quantity       int       `gorm:"not null"`
	UnitPrice      float32   `gorm:"not null"`
	TotalPrice     float32   `gorm:"not null"`
	Comments       string    `gorm:"not null"`
}

//Migrate ProductIn model structure to db
func (ProductOut) migrate() {
	logger.Log.Println("migrating product_out schema")
	db := storage.SqliteConn()
	db.AutoMigrate(ProductOut{})
}

//TableName return name of databse table used for this model
func (ProductOut) TableName() string {
	return "product_out"
}
