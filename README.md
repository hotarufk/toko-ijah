# Toko Ijah

a set of APIs to be used by front-end engineers to develop an application that replace ijah spreadsheet
**Tech Stack**
1. Go 
2. Sqlite 
3. Docker
4. Docker-Compose 

This particular stack used to make sure the code can be setup on any new machine with ease. Also by containering a service it's make the service easily scaled up or scaled down by increasing or decreasing number of container

## Directory Structure
```
toko-ijah
    |--config                           - to store configuration
        |--tpl.go                       - to store configuration for go template
    |--controllers                      - to store package controllers
        |--bulkController.go            - to handle bulk operation
        |--exportController.go          - to handle export operation
        |--importController.go          - to handle import operation
        |--mainController.go            - to handle CRUD operation
        |--reportController.go          - to handle report operation
        |--viewController.go            - to handle CRUD operation
        |--helper.go                    - to store ghelper function to process request
        |--router.go                    - to store routing table
    |--logger                           - to store custom logger
        |--logger.go                    - for loggin into file
    |--models                           - to store package models for object and mysql query
        |--base.go                      - for table base function declaration
        |--product.go                   - for table product
        |--productIn.go                 - for table product_incoming
        |--productOut.go                - for table product_outgoing
    |--parameter                        - to store models for request and response body
        |--requestParameter.go          - to define request body 
        |--responseParameter.go         - to define response body 
        |--validator.go                 - to validate request body based on requestParameter
    |--postman                          - to store sample postman collection and parameter
        |--api.postman_collection.json  - file to be exported as collection in postman
        |--api.postman_environment.json - file to be exported as environment in postman    
    |--sample                           - to store sample csv for import
        |--CatatanBarangKeluar.json     - sample csv file to be used for importing outgoing product data
        |--CatatanBarangMasuk.json      - sample csv file to be used for importing incoming product data
        |--CatatanJumlahBarang.json     - sample csv file to be used for importing product data
    |--storage                          - for initialize database connection
        |--sqlite.go                    - for initialize sqlite database connection
    |--templates                        - for project view template
        |--product.gohtml               - for product html view
        |--productIncoming.gohtml       - for product incoming html view
        |--productOutgoing.gohtml       - for product outgoing html view
    |--docker-compose.yml               - for docker-compose service and config
    |--Dockerfile                       - Dockerfile for Golang
    |--main.go                          - entrypoint for api
```

## Deployment

**Setup**
1. git clone [git@bitbucket.org:hotarufk/toko-ijah.git](git@bitbucket.org:hotarufk/toko-ijah.git)
2. install docker and docker-compose
3. make directory /var/log/toko-ijah/  
4. open terminal and run docker-compose build (service are build), docker-compose up(builds, recreates, attaches to container for service), docker-compose down (stop containers and remove containers etc) See [Docker Documentation](https://docs.docker.com/compose/reference/build/) on how to use it.
5. now your server ready for http:localhost:8080/
