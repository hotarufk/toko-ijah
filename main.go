package main

import (
	"net/http"
	"toko-ijah/controller"
	"toko-ijah/logger"
	"toko-ijah/models"
)

func main() {
	models.Migrate()
	logger.Log.Print("Starting listening")
	logger.Log.Fatal(http.ListenAndServe(":8080", controller.GetRoute()))
}
